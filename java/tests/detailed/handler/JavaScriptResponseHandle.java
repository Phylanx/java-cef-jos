package tests.detailed.handler;

public class JavaScriptResponseHandle {

	private final String responseHandle;
	private String response;

	public JavaScriptResponseHandle(String responseHandle) {
		this.responseHandle = responseHandle;
	}

	@Override
	public String toString() {
		return "responseHandle " + responseHandle;
	}

	public void waitForCall(long maximumMsWait) throws InterruptedException {
		long msStart = System.currentTimeMillis();
		while (response == null) {
			Thread.sleep(100);
			if (System.currentTimeMillis() > (maximumMsWait + msStart)) {
				System.out.println("Timeout, response not given!");
				break;
			}
		}
	}

	public void stopWaiting(String response) {
		this.response = response;
	}

	public String getResponse() {
		return response;
	}
}