package tests.detailed.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefQueryCallback;
import org.cef.handler.CefMessageRouterHandlerAdapter;

public class JavaScriptResponseWaiter extends CefMessageRouterHandlerAdapter {
	private static final String CALL_TEMPLATE = "var returnValue = %1$s"
			+ " ; cefCallback(\"JavaScriptResponseWaiter,%2$s,\" + returnValue);";
	private static final AtomicInteger CURRENT_HANDLE = new AtomicInteger();
	private Map<String, JavaScriptResponseHandle> handles = new HashMap<>();

	public String executeAndWaitForCallback(CefBrowser cefBrowser_, String jsCall, long maximumMsWait)
			throws InterruptedException {
		String uniqueResponseHandle = getUniqueResponseHandle();
		JavaScriptResponseHandle handle = new JavaScriptResponseHandle(uniqueResponseHandle);
		synchronized (handles) {
			if (handles.containsKey(uniqueResponseHandle)) {
				throw new RuntimeException("Duplicate response handle: " + uniqueResponseHandle);
			}
			handles.put(uniqueResponseHandle, handle);
		}
		String call = String.format(CALL_TEMPLATE, jsCall, uniqueResponseHandle);
		cefBrowser_.executeJavaScript(call, "JavaScriptResponseWaiter.java/executeAndWaitForCallback", 55);
		handle.waitForCall(maximumMsWait);
		return handle.getResponse();
	}

	@Override
	public boolean onQuery(CefBrowser browser, CefFrame frame, long queryId, String request, boolean persistent,
			CefQueryCallback callback) {
		if (request.startsWith("JavaScriptResponseWaiter")) {
			// we got a response to handle!
			String handleAndResponse = request.substring("JavaScriptResponseWaiter".length() + 1);
			int indexOf = handleAndResponse.indexOf(",");
			String handle = handleAndResponse.substring(0, indexOf);
			String response = handleAndResponse.substring(indexOf + 1);
			System.out.println(
					"Got response: " + request + "; Response for handle " + handle + "; response = " + response);
			synchronized (handles) {
				JavaScriptResponseHandle javaScriptResponseHandle = handles.get(handle);
				javaScriptResponseHandle.stopWaiting(response);
			}
		}
		return super.onQuery(browser, frame, queryId, request, persistent, callback);
	}

	private static String getUniqueResponseHandle() {
		return "" + CURRENT_HANDLE.incrementAndGet();
	}
}